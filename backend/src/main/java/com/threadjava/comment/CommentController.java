package com.threadjava.comment;

import com.threadjava.comment.dto.CommentDetailsDTO;
import com.threadjava.comment.dto.CommentSaveDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.UUID;
import static com.threadjava.auth.TokenService.getUserId;

@RestController
@RequestMapping("/api/comments")
public class CommentController {
    @Autowired
    private CommentService commentService;

    @GetMapping("/{id}")
    public CommentDetailsDTO get(@PathVariable UUID id) {
        return commentService.getCommentById(id);
    }

    @PostMapping
    public CommentDetailsDTO post(@RequestBody CommentSaveDTO commentDTO) {
        commentDTO.setUserId(getUserId());
        return commentService.create(commentDTO);
    }
}
