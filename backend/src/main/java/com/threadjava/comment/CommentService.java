package com.threadjava.comment;

import com.threadjava.comment.dto.CommentDetailsDTO;
import com.threadjava.comment.dto.CommentSaveDTO;
import com.threadjava.post.PostsRepository;
import com.threadjava.users.UsersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.UUID;

@Service
public class CommentService {
    @Autowired
    private CommentRepository commentRepository;
    @Autowired
    private UsersRepository usersRepository;
    @Autowired
    private PostsRepository postsRepository;

    public CommentDetailsDTO getCommentById(UUID id) {
        return commentRepository.findById(id)
                .map(CommentMapper.MAPPER::commentToCommentDetailsDTO)
                .orElseThrow();
    }

    public CommentDetailsDTO create(CommentSaveDTO commentDTO) {
        var comment = CommentMapper.MAPPER.commentSaveDTOToModel(commentDTO);
        var postCreated = commentRepository.save(comment);
        return CommentMapper.MAPPER.commentToCommentDetailsDTO(postCreated);
    }
}
