package com.threadjava.comment.dto;

import com.threadjava.users.dto.UserShortDTO;
import lombok.Data;

import java.util.UUID;

@Data
public class CommentDetailsDTO {
    private UUID id;
    private String body;
    private UserShortDTO user;
    private UUID postId;
}
