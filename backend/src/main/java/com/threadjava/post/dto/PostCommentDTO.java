package com.threadjava.post.dto;

import lombok.Data;
import java.util.UUID;

@Data
public class PostCommentDTO {
    private UUID id;
    private String body;
    private PostUserDTO user;
}
