package com.threadjava.post.dto;

import com.threadjava.image.dto.ImageDTO;
import com.threadjava.users.dto.UserShortDTO;
import lombok.Data;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Data
public class PostDetailsDTO {
    private UUID id;
    private String body;
    private ImageDTO image;
    private UserShortDTO user;
    private Date createdAt;
    private Date updatedAt;
    public long likeCount;
    public long dislikeCount;
    public long commentCount;
    private List<PostCommentDTO> comments = new ArrayList<>();
//    @Getter @Setter public List<PostReactionDTO> reactions = new ArrayList<>();
}
