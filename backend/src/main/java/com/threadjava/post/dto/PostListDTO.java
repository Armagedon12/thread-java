package com.threadjava.post.dto;

import com.threadjava.image.dto.ImageDTO;
import lombok.Data;
import java.util.Date;
import java.util.UUID;

@Data
public class PostListDTO {
    private UUID id;
    private String body;
    private long likeCount;
    private long dislikeCount;
    private long commentCount;
    private Date createdAt;
    private ImageDTO image;
    private PostUserDTO user;
}
