package com.threadjava.post.dto;

import com.threadjava.image.dto.ImageDTO;
import lombok.Data;
import java.util.UUID;

@Data
public class PostUserDTO {
    private UUID id;
    private String username;
    private ImageDTO image;
}
