package com.threadjava.postReactions;

import com.threadjava.postReactions.dto.ReceivedPostReactionDTO;
import com.threadjava.postReactions.dto.ResponsePostReactionDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class PostReactionService {

    @Autowired
    private PostReactionsRepository postReactionsRepository;

    public Optional<ResponsePostReactionDTO> setReaction(ReceivedPostReactionDTO postReactionDTO) {

        var reaction = postReactionsRepository.getPostReaction(postReactionDTO.getUserId(), postReactionDTO.getPostId());

        if (reaction.isPresent()) {
            var react = reaction.get();
            if (react.getIsLike() == postReactionDTO.getIsLike()) {
                postReactionsRepository.deleteById(react.getId());

                return Optional.empty();
            } else {
                react.setIsLike(postReactionDTO.getIsLike());
                var result = postReactionsRepository.save(react);

                return Optional.of(PostReactionMapper.MAPPER.reactionToPostReactionDTO(result));
            }
        } else {
            var postReaction = PostReactionMapper.MAPPER.dtoToPostReaction(postReactionDTO);
            var result = postReactionsRepository.save(postReaction);

            return Optional.of(PostReactionMapper.MAPPER.reactionToPostReactionDTO(result));
        }
    }
}
