package com.threadjava.postReactions.dto;

import lombok.Data;
import java.util.UUID;

@Data
public class PostReactionDTO {
    private UUID id;
    private Boolean isLike;
}
