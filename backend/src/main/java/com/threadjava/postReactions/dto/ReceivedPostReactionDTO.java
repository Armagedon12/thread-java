package com.threadjava.postReactions.dto;

import lombok.Data;

import java.util.UUID;

@Data
public class ReceivedPostReactionDTO {
    private UUID postId;
    private UUID userId;
    private Boolean isLike;
}
