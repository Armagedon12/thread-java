package com.threadjava.users;

import com.threadjava.users.dto.UserDetailsDTO;
import com.threadjava.users.model.User;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper
public interface UserMapper {
    UserMapper MAPPER = Mappers.getMapper( UserMapper.class );

    @Mapping(source = "avatar", target = "image")
    UserDetailsDTO userToUserDetailsDTO(User user);
}
