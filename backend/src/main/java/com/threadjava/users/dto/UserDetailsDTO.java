package com.threadjava.users.dto;

import com.threadjava.image.dto.ImageDTO;
import lombok.Data;
import java.util.UUID;

@Data
public class UserDetailsDTO {
    private UUID id;
    private String email;
    private String username;
    private ImageDTO image;
}
