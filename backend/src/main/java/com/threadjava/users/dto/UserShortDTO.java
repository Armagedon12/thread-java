package com.threadjava.users.dto;

import lombok.*;

import java.util.UUID;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserShortDTO {
    private UUID id;
    private String username;
}
