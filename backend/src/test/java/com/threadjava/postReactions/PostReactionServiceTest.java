package com.threadjava.postReactions;

import com.threadjava.postReactions.dto.ReceivedPostReactionDTO;
import com.threadjava.postReactions.dto.ResponsePostReactionDTO;
import com.threadjava.postReactions.model.PostReaction;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
class PostReactionServiceTest {

    @Mock
    private PostReactionsRepository repository;
    @Mock
    private ReceivedPostReactionDTO reactionDTO;
    @Mock
    private PostReaction reaction;
    @InjectMocks
    private PostReactionService service;
    private UUID userID = UUID.randomUUID();
    private UUID postID = UUID.randomUUID();
    private UUID reactionID = UUID.randomUUID();

    @Test
    public void setReaction_shouldRemoveReactionOnSecondClick() {
        doReturn(true).when(reactionDTO).getIsLike();
        doReturn(postID).when(reactionDTO).getPostId();
        doReturn(userID).when(reactionDTO).getUserId();
        doReturn(true).when(reaction).getIsLike();
        doReturn(reactionID).when(reaction).getId();
        doReturn(Optional.of(reaction)).when(repository).getPostReaction(userID, postID);

        Optional<ResponsePostReactionDTO> actual = service.setReaction(reactionDTO);

        assertEquals(Optional.empty(), actual);
        verify(repository).deleteById(reactionID);
    }

//    @Test
//    public void setReaction_shouldAddReactionOnFirstClick() {
//        doReturn(true).when(reactionDTO).getIsLike();
//        doReturn(postID).when(reactionDTO).getPostId();
//        doReturn(userID).when(reactionDTO).getUserId();
//        doReturn(true).when(reaction).getIsLike();
//        doReturn(reactionID).when(reaction).getId();
//        doReturn(Optional.empty()).when(repository).getPostReaction(userID, postID);
//
//        Optional<ResponsePostReactionDTO> actual = service.setReaction(reactionDTO);
//
//        assertEquals(Optional.empty(), actual);
//        verify(repository).save(reaction);
 //   }
}
